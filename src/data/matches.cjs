const fs = require('fs');
const csvjson = require('csvjson');
const path = require('path');
const pathForMatch = path.join(__dirname, "./matches.csv");


const matchDataCSV = fs.readFileSync(pathForMatch, { encoding : 'utf8'});

const options = {
  delimiter : ',', // optional
  quote     : '"' // optional
};

const matchData=csvjson.toObject(matchDataCSV, options);

module.exports=matchData;