const fs = require('fs');
const csvjson = require('csvjson');
const path = require('path');
const pathForDeliveries = path.join(__dirname,"./deliveries.csv");


const deliveriesCSV = fs.readFileSync(pathForDeliveries,{encoding:'utf8'});

const options = {
  delimiter : ',', // optional
  quote     : '"' // optional
};

const deliveriesData=csvjson.toObject(deliveriesCSV, options);

module.exports=deliveriesData;