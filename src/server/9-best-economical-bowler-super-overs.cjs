module.exports = function bestEconomyInSuperOver(deliveryData) {

    let listOfSuperOver = deliveryData.filter((match) => match.is_super_over !== "0");

    let bowlersHasBestEconomyInSO = topEconomicalBowlerInSO(listOfSuperOver);
 
    return bowlersHasBestEconomyInSO;
};

function topEconomicalBowlerInSO(listOfSuperOver) {
    
    let totalNumberOfBall = {};
    let totalRunByBaller = listOfSuperOver.reduce((acc,match)=>{
        const bowler = match.bowler;
        if (totalNumberOfBall.hasOwnProperty(bowler)) {
            totalNumberOfBall[bowler] += 1;
        } else {
            totalNumberOfBall[bowler] = 1;
        }

        if (acc.hasOwnProperty(bowler)) {
            acc[bowler] =
                acc[bowler] + parseInt(match.total_runs);
        } else {
            acc[bowler] = parseInt(match.extra_runs);
        }
        return acc;
    },{});

    let keys = Object.keys(totalNumberOfBall);
   
    let bestEconomy = keys.reduce((acc,player)=>{
        let overs = totalNumberOfBall[player]/6;
        let runs = totalRunByBaller[player];
        acc[player]=runs/overs;
        return acc;
    },{});

    return bestEconomy;
}
