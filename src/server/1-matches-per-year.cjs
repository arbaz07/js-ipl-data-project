function matchesPlayedPerYear(matchesData) {
    const result = matchesData.reduce((accumulator, currentMatch) => {
        if (accumulator.hasOwnProperty(currentMatch.season)) {
            accumulator[currentMatch.season] += 1;
        } else {
            accumulator[currentMatch.season] = 1;
        }
        return accumulator;
    }, {});

    return result;
}

module.exports = matchesPlayedPerYear;
