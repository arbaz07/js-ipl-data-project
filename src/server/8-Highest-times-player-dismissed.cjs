module.exports = function highestTimeBatsmanGotOutByABowler(deliveries) {
    let batsmanGotOutByBowlers = deliveries.reduce((acc,delivery)=>{

        if(acc.hasOwnProperty(delivery.player_dismissed)){
            if(acc[delivery.player_dismissed].hasOwnProperty(delivery.bowler)){
                acc[delivery.player_dismissed][delivery.bowler]+=1;
            }else{
                acc[delivery.player_dismissed][delivery.bowler]=1;
            }
        }else{
            acc[delivery.player_dismissed] = {[delivery.bowler]:1};
        }
        return acc;
    },{});

    let allBatsmen = Object.keys(batsmanGotOutByBowlers);
    let allBowlersTookWicket = Object.values(batsmanGotOutByBowlers);

    let sortedData = allBowlersTookWicket.reduce((acc,bowlers,index)=>{

        let arr = Object.entries(bowlers)
            .sort(([,item1], [,item2]) => item2 - item1).splice(0,1).flat();
            
        acc[allBatsmen[index]]={[arr.flat()[0]]:arr.flat()[1]}
        return acc;

    },{});

    return sortedData;
};
