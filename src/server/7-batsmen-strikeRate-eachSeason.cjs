module.exports = function batsmanStrinkeRateForAllYears(
    matchesData,
    deliveriesData
) {
    let seasons = new Set();
    for (let match of matchesData){
        seasons.add(match.season);
    }
    let allSeason=Array.from(seasons); 
     
    let strikeRatePerYearForAllBatsman = allSeason.reduce(
        (accumulator, year) => {
            let deliveriesPerYear = deliveriesDoneInOneYear(
                matchesData,
                deliveriesData,
                year
            );

            accumulator[year] = strikeRatePerYear(deliveriesPerYear);
            return accumulator;
        },
        {}
    );
    return strikeRatePerYearForAllBatsman;
};

function deliveriesDoneInOneYear(matches, deliveries, year) {
    let matchPlayedInOneYear = matches.reduce((acc, game) => {
        if (game.season == year) {
            acc[game.id] = year;
        }
        return acc;
    }, {});

    let keysOfMatchPlayedInSeason = Object.keys(matchPlayedInOneYear);

    let deliveriesDoneInSeason = deliveries.filter((delivery) =>
        keysOfMatchPlayedInSeason.includes(delivery.match_id)
    );
    return deliveriesDoneInSeason;
}

function strikeRatePerYear(dataPerYear) {
    let batsmanDeatails = dataPerYear.reduce((acc, match) => {
        let temp = {};
        const batsman = match.batsman;
        if (acc.hasOwnProperty(batsman)) {
            acc[batsman].ball_faced += 1;
            acc[batsman].batsman_runs += parseInt(match.batsman_runs);
        } else {
            temp.ball_faced = 1;
            temp.batsman_runs = parseInt(match.batsman_runs);
            acc[batsman] = temp;
        }
        return acc;
    }, {});

    let batsmenNames = Object.keys(batsmanDeatails);

    
    let batsmanStrikeRate = batsmenNames.reduce(
        (acc, val, index, batsmanArray) => {
            let balls = batsmanDeatails[batsmanArray[index]].ball_faced;
            let runs = batsmanDeatails[batsmanArray[index]].batsman_runs;
            acc[batsmanArray[index]] = ((runs / balls) * 100).toPrecision(4);
            return acc;
        },
        {}
    );

    return batsmanStrikeRate;
}


// let batsmanStrikeRate = batsmenNames.reduce((acc,batsman)=>{
    //     let currDeatail = batsmanDeatails[batsman];
    //     let keysOfCurrDeatail = Object.keys(currDeatail);
    //     let runs = currDeatail[keysOfCurrDeatail[1]];
    //     let balls = currDeatail[keysOfCurrDeatail[0]];
    //     acc[batsman]= runs/balls;
    //     return acc;
    //     //acc[batsman]=runs/balls;

    // },{});

