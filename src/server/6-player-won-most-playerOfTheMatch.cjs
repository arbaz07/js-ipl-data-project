//Find a player who has won the highest number of Player of the Match awards for each season
function mostManOfTheMatchWinner(matchData) {

    let ansObj = matchData.reduce((acc,match)=>{
        const season=match.season;
        const playerOfTheMatch = match.player_of_match;
        if(acc.hasOwnProperty(match.season)){
            if(acc[season].hasOwnProperty(playerOfTheMatch)){
                acc[season][playerOfTheMatch]+=1;
            }else{
                acc[season][playerOfTheMatch]=1;
            }
        }else{
            acc[season]={[playerOfTheMatch]:1};
        }
        return acc;
    },{});

    let asn = Object.keys(ansObj).reduce((acc,season)=>{
        acc[season]= sortData(ansObj[season]);
        return acc;
    },{})

    return asn;
}

function sortData(listOfPlayerOfTheMatch) {
   
    let sortedListOfPOM = Object.entries(listOfPlayerOfTheMatch)
        .sort(([, item1], [, item2]) => item2 - item1).slice(0,1)
        .reduce((rem, [key, val]) => ({ ...rem, [key]: val }), {});

    return sortedListOfPOM;
}

module.exports = mostManOfTheMatchWinner;
