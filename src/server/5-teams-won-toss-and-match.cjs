function teamWonTossAsWellMatch(matchesData) {
    let result = matchesData.reduce((acc, match) => {
        const matchWinner = match.winner; 
        if (match.toss_winner == match.winner) {
            if (acc.hasOwnProperty(matchWinner)) {
                acc[matchWinner] += 1;
            } else {
                acc[matchWinner] = 1;
            }
        }
        return acc;
    }, {});

    return result;
}

module.exports = teamWonTossAsWellMatch;
