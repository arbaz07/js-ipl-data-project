function matchesWonPerTeamPerYear(matchesData) {
    let result = matchesData.reduce((accumulator, match) => {
        if (accumulator.hasOwnProperty(match.season)) {
            if (accumulator[match.season].hasOwnProperty(match.winner)) {
                accumulator[match.season][match.winner] =
                    accumulator[match.season][match.winner] + 1;
            } else {
                accumulator[match.season][match.winner] = 1;
            }
        } else {
            accumulator[match.season] = { [match.winner]: 1 };
        }
        return accumulator;
    }, {});
    return result;
}

module.exports = matchesWonPerTeamPerYear;
