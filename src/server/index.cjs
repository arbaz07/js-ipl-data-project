const path = require('path');
const fs = require('fs');
const matchPath = path.join(__dirname,"../data/matches.cjs");
const deliveriesPath = path.join(__dirname,"../data/deliveries.cjs");

const matcheData = require(matchPath);
const deliveriesData = require(deliveriesPath);


console.time();
const calculateMatchesPerYear = require("./1-matches-per-year.cjs"); 
const result1 = calculateMatchesPerYear(matcheData);
console.log(result1);
console.timeEnd();
// fs.writeFileSync("../public/1-matches-per-year.json", JSON.stringify(result1, null, 4));


console.time();
const matchesWonPerTeamPerYear = require("./2-matches-won-per-team-per-year.cjs"); 
const result2 = matchesWonPerTeamPerYear(matcheData);
console.log(result2);
console.timeEnd();
// fs.writeFileSync("../public/2-matches-won-per-team-per-year.json", JSON.stringify(result2, null, 4));


console.time();
const extraRunPerTeamInYear = require("./3-extra-run-conceded-per-team-2016.cjs"); 
const result3 = extraRunPerTeamInYear(matcheData,deliveriesData,'2016');
console.log(result3);
console.timeEnd();
// fs.writeFileSync("../public/3-extra-run-conceded-per-team-2016.json", JSON.stringify(result3, null, 4));


console.time();
const topTenEconomicalBowlers = require("./4-economical-Bowlers-2015.cjs"); 
const result4 = topTenEconomicalBowlers(matcheData,deliveriesData,'2016');
console.log(result4);
console.timeEnd();   
// fs.writeFileSync("../public/4-economical-Bowlers-year.json", JSON.stringify(result4, null, 4));


console.time();
const teamWonTossAsWellMatch = require("./5-teams-won-toss-and-match.cjs"); 
const result5 = teamWonTossAsWellMatch(matcheData);
// fs.writeFileSync("../public/5-teams-who-won-toss-and-match.json", JSON.stringify(result5, null, 4));
console.log(result5);
console.timeEnd();

console.time();
const mostManOfTheMatchWinner = require("./6-player-won-most-playerOfTheMatch.cjs"); 
const result6 = mostManOfTheMatchWinner(matcheData);
// fs.writeFileSync("../public/6-player-won-most-playerOfTheMatch.json", JSON.stringify(result6, null, 4));
console.log(result6);
console.timeEnd();


console.time();
const batsmanStrinkeRateForAllYears = require("./7-batsmen-strikeRate-eachSeason.cjs"); 
const result7 = batsmanStrinkeRateForAllYears(matcheData,deliveriesData);
// fs.writeFileSync("../public/7-batsmen-strikeRate-eachSeason.json", JSON.stringify(result7, null, 4));
console.log(result7);
console.timeEnd();



console.time();
const highestTimeBatsmanGotOutByABowler = require("./8-Highest-times-player-dismissed.cjs"); 
const result8 = highestTimeBatsmanGotOutByABowler(deliveriesData);
    // fs.writeFileSync("../public/8-Highest-times-player-dismissed.json", JSON.stringify(result8, null, 4));
console.log(result8);
console.timeEnd();



console.time();
const bestEconomicalBowlerInSuperOver = require("./9-best-economical-bowler-super-overs.cjs"); 
const result9 = bestEconomicalBowlerInSuperOver(deliveriesData);
// fs.writeFileSync("../public/9-best-economical-bowler-super-overs.json", JSON.stringify(result9, null, 4));
console.log(result9)
console.timeEnd();
    
