function topEconomicalBalllers(matches, deliveries, year) {

    let matchPlayedInOneYear = matches.reduce((acc,game) => {
        if(game.season == year){
            acc[game.id]=year;
        }
        return acc;
    },{});

    let keysOfMatchPlayedInSeason=Object.keys(matchPlayedInOneYear);

    let deliveriesDoneInSeason = deliveries.filter((delivery)=>
        keysOfMatchPlayedInSeason.includes(delivery.match_id)
    );

    let totalRunByBaller={};
    let totalNumberOfBall = deliveriesDoneInSeason.reduce((acc, delivery) => {
            const bowler = delivery.bowler;
            const runs = delivery.total_runs;
            if (acc.hasOwnProperty(bowler)) {
                acc[bowler] += 1;
            } else {
                acc[bowler] = 1;
            }

            if (totalRunByBaller.hasOwnProperty(bowler)) {
                totalRunByBaller[bowler] += parseInt(runs);
            } else {
                totalRunByBaller[bowler] = parseInt(runs);
            }
            return acc;
        },
        {}
    );

    for (let item in totalRunByBaller) {
        let oversPerPlayer = totalNumberOfBall[item] / 6;
        totalRunByBaller[item] = (totalRunByBaller[item] / oversPerPlayer).toPrecision(3);
      
    }

    let sortedBallingEconomy = Object.entries(totalRunByBaller)
    .sort(([, item1], [, item2]) => item1 - item2)
    .slice(0,10)
    .reduce((rem, [key, val]) => ({ ...rem, [key]: val }), {});
    
    return sortedBallingEconomy;

}

module.exports = topEconomicalBalllers;
