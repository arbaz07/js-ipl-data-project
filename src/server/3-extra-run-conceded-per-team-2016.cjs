function extraRunPerTeamInYear(matches, deliveries, year) {
    
    let matchDeliveriesDoneInYear = deliveriesDoneInOneYear(matches,deliveries,year);

    let extraRun = matchDeliveriesDoneInYear.reduce((acc, delivery) => {
        const battingTeam = delivery.batting_team;
        const extraRunnGiven = delivery.extra_runs;
        if (acc.hasOwnProperty(battingTeam)) {
            acc[battingTeam] =
                acc[battingTeam] + parseInt(extraRunnGiven);
        } else {
            acc[battingTeam] = parseInt(extraRunnGiven);
        }
        return acc;
    }, {});
    return extraRun;
}



function deliveriesDoneInOneYear(matches, deliveries, year) {
    let matchPlayedInOneYear = matches.reduce((acc, game) => {
        if (game.season == year) {
            acc[game.id] = year;
        }
        return acc;
    }, {});

    let keysOfMatchPlayedInSeason = Object.keys(matchPlayedInOneYear);

    let deliveriesDoneInSeason = deliveries.filter((delivery) =>
        keysOfMatchPlayedInSeason.includes(delivery.match_id)
    );
    return deliveriesDoneInSeason;
}

module.exports = extraRunPerTeamInYear;
